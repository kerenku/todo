<?php

use Illuminate\Database\Seeder;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                    'name' => 'esther',
                    'email' => 'esther@gmail.com',
                    'password' =>Hash::make('12345678'),
                    'role' => 'Employees',
                    'created_at' => date('Y-m-d G:i:s'),
                    ],
                    [
                    'name' => 'aviad',
                    'email' => 'aviad@gmail.com',
                    'password' =>Hash::make('12345678'),
                    'role' => 'Employees',
                    'created_at' => date('Y-m-d G:i:s'),
                ],
            ]);
    }
}
