@extends('layouts.app')
@section('content')
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

<div class="container">
    <br/><br/>
    <h1> create a new todo </h1>
<form method = 'post' action = "{{action('TodoController@store')}}">
{{csrf_field()}}

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

    


<div class = "form-group">
<label for = "title" > What would you like todo? </label>
<input type = "text" class ="form-control" name = "title">
</div>

<div class ="form-group">
<input type= "submit" class = "form-control" name= "submit" value = "save">
</div>
</form>

@endsection
